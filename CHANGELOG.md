# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [16.0.3](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/standard/compare/v0.0.27...v16.0.3) (2021-06-14)

### [0.0.27](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/standard/compare/v0.0.26...v0.0.27) (2021-06-13)

### [0.0.26](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/standard/compare/v0.0.25...v0.0.26) (2021-06-13)

### [0.0.25](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/standard/compare/v0.0.24...v0.0.25) (2021-06-13)

### [0.0.24](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/standard/compare/v0.0.23...v0.0.24) (2021-06-13)

### 0.0.23 (2021-05-13)
